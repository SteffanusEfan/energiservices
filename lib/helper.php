<?php

class helper 
{
	public static function getUserName($id) {
		$db = Db::init();
		$u = $db->users;
		$name = '';
		if(strlen(trim($id)) > 0) {
			$uu = $u->findone(array('_id' => new MongoId($id)));
			$name = $uu['name'];
		}
		
		return $name;
	}
	
	public static function getLogoProduct() {
		$db = Db::init();
		$p = $db->product;
		$q = array('client_id' => CLIENTID);
		$col = $p->find($q)->sort(array('position' => 1));
		
		return $col;
	}	
	
	public static function getHomeContent($catid) {
		$db = Db::init();
		$c = $db->content;
		$q = array(
			'client_id' => CLIENTID,
			'category_id' =>  $catid
		);
		$col = $c->findone($q);
		
		if(isset($col['_id']))
			return $col;
		
		return array();
	}
	
	public static function getPreferences(){
		$db = Db::init();
		$pref = $db->preference;
		$data = $pref->findOne(array('client_id' => CLIENTID));
		return $data;
	}
	
	public static function getClient(){
		$db = Db::init();
		$pref = $db->client;
		$data = $pref->findOne(array('_id' => new MongoId(CLIENTID)));
		return $data;
	}
	
	public static function varCheck($var){
		$ret = "<pre>".print_r($var,1)."</pre>";
		
		return $ret;
	}
	
	public static function getImage($imageid){
		$db = Db::init();
		$image = $db->image;
		$where = array('_id' => new MongoId(trim($imageid)));
		$data = $image->findOne($where);
		
		return $data;
	}
	
	public static function delImage($imageid,$folder){
		$db = Db::init();
		$image = $db->image;
		$where = array('_id' => new MongoId(trim($imageid)));
		$data = $image->findOne($where);
		$fname = $data['filename'];
		$image->remove($where);
		unlink('/public/upload/'.$folder.'/'.$fname);
		
		return true;
	}
	
	public static function findexts($filename) {
		$path_parts = pathinfo($filename);
		return $path_parts['extension'];
	}
	
	public static function ambilNamaBulan($n) {
		switch ($n) {
			default: return 'Januari'; break;
			case 2: return 'Februari'; break;
			case 3: return 'Maret'; break;	
			case 4: return 'April'; break;
			case 5: return 'Mei'; break;
			case 6: return 'Juni'; break;
			case 7: return 'Juli'; break;
			case 8: return 'Agustus'; break;
			case 9: return 'September'; break;
			case 10: return 'Oktober'; break;
			case 11: return 'November'; break;
			case 12: return 'Desember'; break;
		}
	}
	
	public static function limitString($string, $limit = 100) 
	{
	    // Return early if the string is already shorter than the limit
	    if(strlen($string) < $limit) {return $string;}
	
	    $regex = "/(.{1,$limit})\b/";
	    preg_match($regex, $string, $matches);
	    return $matches[1].'...';
	}
	
	public static function showDialog()
	{		
		echo '<div id="deleteDialog">';               
		echo '<div class="mws-dialog-inner"><p></p></div>';	
		echo '</div>';
	}
	
	public static function deleteTmpDialog()
	{		
		echo '<div id="deleteTmpDialog">';               
		echo '<div class="mws-dialog-inner"><p></p></div>';	
		echo '</div>';
	}
	
	public static function showFindDialog()
	{		
		echo '<div id="findDialog">';               
		echo '<div class="mws-dialog-inner"><div></div></div>';	
		echo '</div>';
	}
	
	public static function showFormDialog($id="")
	{	
		echo '<div id="mws-form-dialog">';
        echo '<form id="mws-validate" class="mws-form">';
        echo '<div id="mws-validate-error" class="mws-form-message error" style="display:none;"></div>';
        echo '<div class="mws-form-inline">';
        echo '<div class="mws-form-row">';
        echo '<label class="mws-form-label"></label>';
        echo '<div class="mws-form-item">';
        echo '<input id="tgl" type="text" name="tgl" class="required tgl mws-datepicker" value="'.date('d-m-Y', time()).'">';
        echo '</div>';
        echo '</div>';        
        echo '</div>';
        echo '</form>';
    	echo '</div>';
	}
	
	public static function randomKey()
    {
        $crypt = new CkCrypt2() ;

	    $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
	    if ($success != true) {
	        printf("%s\n",$crypt->lastErrorText());
	        return "";
	    }

		$crypt->put_CryptAlgorithm("aes");
	    $crypt->put_CipherMode("cbc");
	    $crypt->put_KeyLength(256);
	    $crypt->put_PaddingScheme(0);
	    $crypt->put_EncodingMode("hex");

    	return $crypt->genRandomBytesENC(32);               
    }
	
	public static function passwordHash($pwd)
	{
		$salt = 'PHOTOclient798dig%%44$$33**_';
		$salt = md5($salt.$pwd);
				
		$options = array( 'cost' => 11, 'salt' => $salt );
		$pwd = password_hash($pwd, PASSWORD_BCRYPT, $options);
		return $pwd;
	}
	
}

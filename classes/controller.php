<?php

class controller
{
	var $css;
	var $js;
	var $playlist = array();
	var $dta;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();						
	}
	
	protected function setSessionLanguage() {
		$db = Db::init();
		$p = $db->preference;
		$q = array(
			'client_id' => CLIENTID
		);
		$col = $p->findone($q);
				
		if(isset($col['language_id'])) {
			if(strlen(trim($col['language_id'])) > 0)
				$_SESSION['language'] = $col['language_id'];
		}
	}
	
	protected function before()
	{
		
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
		(include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
	
	protected function getSort()
	{
	    $sort = "time_created";
		if(isset($_GET['sort']))
			$sort = $_GET['sort'];
		
		return $sort;
	}
	
	protected function getSortType()
	{
		$tipesort = 'ASC';
		if (isset($_GET['tipesort'])){
		
			if($_GET['tipesort'] == "ASC")
				$tipesort = 'ASC';
			else
				$tipesort = 'DESC';
		}
		
		return $tipesort;
	}
	
	protected function setSort($sort)
	{
		if(strlen($sort) < 1)
			$sort = 'time_created';
		
		$setsort = array($sort => -1);
		if (isset($_GET['tipesort']))
		{
			if($_GET['tipesort'] == "ASC")
				$setsort = array($sort => 1);
			else
				$setsort = array($sort => -1);
		}
		return $setsort;
	}
	
	protected function redirect($page)
	{
		header( 'Location: '.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{		
		$css[] = '<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/bootstrap.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/style.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/swiper.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/demos/construction/construction.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/dark.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/font-icons.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/animate.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/magnific-popup.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/demos/construction/css/fonts.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/css/responsive.css" type="text/css" />';
		$css[] = '<link rel="stylesheet" href="/public/demos/construction/css/colors.css" type="text/css" />';
		
		$js[] = '/public/js/jquery.js';
		$js[] = '/public/js/plugins.js';
		$js[] = '/public/js/functions.js';
		
		
		
		if(isset($_SESSION['language'])) {
			if(strlen(trim($_SESSION['language'])) == 0)
				$this->setSessionLanguage();			
		}
		else
			$this->setSessionLanguage();
		//--------------Menu--------------
		$db = Db::init();
		$menu = $db->menu;
		$q = array(
				'client_id'=> CLIENTID,
				'_id' => array('$ne'=>new MongoId('570b5d9e1d85e91b338b4567')),
				'detail.language_id'=> trim($_SESSION['language'])
				
		);
		$dmenu = $menu->find($q);
		//------------end Menu------------		
		
		
		$m = explode("/", $view);
		$mm = explode(".", $m[1]);
		$method = $mm[0];
		
		$content = $this->getView(DOCVIEW.$view, $var);		
		$controller = $group;
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);	
		//include(DOCVIEW."template/template.php");
		//include(DOCVIEW."template/comingsoon.php");
		
        if(strpos($_SERVER['HTTP_HOST'], 'dev') !== false)
            include(DOCVIEW."template/template.php");
        else
        	include(DOCVIEW."template/comingsoon.php");
	}
}
?>
<?php

class content_controller extends controller{
	
	public function index(){
		$id = $_GET['id'];
		$db = Db::init();
		$category = $db->category;
		$colcat = $category->findOne(array('menu_id' => trim($id),'client_id' => CLIENTID,'detail.language_id' => $_SESSION['language']));
		$idcat = $colcat['_id'];
				
		
		$content = $db->content;
		$p = array(
			'category_id' => trim($idcat),
			'client_id' => CLIENTID,
			'content.language_id' => $_SESSION['language']	
		);
		
		$col = $content->find($p)->sort(array('time_created'=>1)) ;
		
	
		$var = array(
			'data' => $col,
			'idmenu' => $id
		);
		
		$this->render('content', 'content/index.php', $var);
		//$view = $this->getView(DOCVIEW."content/index.php", $col);
		
	}
}

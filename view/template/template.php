<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	
	<?php
			$datapreference = helper::getClient();
			$dttitle = '';
			$dtemail = '';
			$dtphone = '';
			if(isset($datapreference['name']))
				$dttitle = $datapreference['name'];
			
			$datapreference = helper::getPreferences();
			if(isset($datapreference['email']))
				$dtemail = $datapreference['email'];
			if(isset($datapreference['phone']))
				$dtphone = $datapreference['phone'];
	?>
	<!-- Stylesheets
	============================================= -->
	<!--
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="style.css" type="text/css" />
		<link rel="stylesheet" href="css/swiper.css" type="text/css" />-->
	

	<!-- Construction Demo Specific Stylesheet -->
	<!--<link rel="stylesheet" href="demos/construction/construction.css" type="text/css" />-->
	<!-- / -->

	<!--
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
		<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
		<link rel="stylesheet" href="css/animate.css" type="text/css" />
		<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	
		<link rel="stylesheet" href="demos/construction/css/fonts.css" type="text/css" />
	
		<link rel="stylesheet" href="css/responsive.css" type="text/css" />-->
	
	
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!--<link rel="stylesheet" href="demos/construction/css/colors.css" type="text/css" />-->

	<!-- Document Title
	============================================= -->
	<?php
		if(isset($css))
		{
			foreach($css as $sc)
			{	
				echo $sc;	
			}
		}
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $dttitle; ?></title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
		<div id="top-bar">

			<div class="container clearfix">

				<div class="col_half nobottommargin clearfix">

					<!-- Top Social
					============================================= -->
					<div id="top-social">
						<ul>
							<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="#" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
							<li><a href="#" class="si-dribbble"><span class="ts-icon"><i class="icon-dribbble"></i></span><span class="ts-text">Dribbble</span></a></li>
							<li><a href="#" class="si-github"><span class="ts-icon"><i class="icon-github-circled"></i></span><span class="ts-text">Github</span></a></li>
							<li><a href="#" class="si-pinterest"><span class="ts-icon"><i class="icon-pinterest"></i></span><span class="ts-text">Pinterest</span></a></li>
							<li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
						</ul>
					</div><!-- #top-social end -->

				</div>

				<div class="col_half fright col_last clearfix nobottommargin">

					<!-- Top Links
					============================================= -->
					<!--
					<div class="top-links">
											<ul>
												<li><a href="index.html">Locations</a>
													<ul>
														<li><a href="#">San Francisco</a></li>
														<li><a href="#">London</a></li>
														<li><a href="#">Amsterdam</a></li>
													</ul>
												</li>
												<li><a href="faqs.html">FAQs</a></li>
												<li><a href="contact.html">Contact</a></li>
											</ul>
										</div>-->
					<!-- .top-links end -->

				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">

				<!-- Logo
				============================================= -->
				<div id="logo">
					<?php
						$db = Db::init();
						$preference = helper::getPreferences();	
						echo '<a href="/welcome/index" class="standard-logo"><img src="/showfile/show?namafile='.$preference['logo'].'" alt="Canvas Logo"></a>';		
					?>
					<!--
					<a href="index.html" class="standard-logo"><img src="demos/construction/images/logo.png" alt="Canvas Logo"></a>
										<a href="index.html" class="retina-logo"><img src="demos/construction/images/logo@2x.png" alt="Canvas Logo"></a>-->
					
				</div><!-- #logo end -->

				<ul class="header-extras">
					<li>
						<i class="i-plain icon-call nomargin"></i>
						<div class="he-text">
							Call Us
							<span style="color:#00A550"><?php echo $dtphone; ?></span>
						</div>
					</li>
					<li>
						<i class="i-plain icon-line2-envelope nomargin"></i>
						<div class="he-text">
							Email Us
							<span style="color:#00A550"><?php echo $dtemail; ?></span>
						</div>
					</li>
					<!--
					<li>
											<i class="i-plain icon-line-clock nomargin"></i>
											<div class="he-text">
												We'are Open
												<span>Mon - Sat, 10AM to 6PM</span>
											</div>
										</li>-->
					
				</ul>

			</div>

			<div id="header-wrap">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="with-arrows style-2 center">

					<div class="container clearfix">

						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

						<ul>
							<li><a href="/welcome/index"><div>Home</div></a></li>
							<?php 
									foreach ($dmenu as $d){
									$name = "";
									foreach($d['detail'] as $dd ){
										if ($dd['language_id'] == $_SESSION['language']){
											$name = $dd['name'];
											break;	
										}
										
									}
									echo '<li><a href="/content/index?id='.$d['_id'].'">'.$name.'</a></li>';
								}
							?>		
							<li><a href="/contact/index"><div>Contact Us</div></a></li>
						</ul>
					</div>

				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->

		<?php echo $content; ?>

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">
			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2016 Energy Services.<br>
						<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
					</div>

					<div class="col_half col_last tright">
						<div class="copyrights-menu copyright-links clearfix">
							<a href="/welcome/index">Home</a>/
							<?php 
									foreach ($dmenu as $d){
									$name = "";
									foreach($d['detail'] as $dd ){
										if ($dd['language_id'] == $_SESSION['language']){
											$name = $dd['name'];
											break;	
										}
										
									}
									echo '<a href="/content/index?id='.$d['_id'].'">'.$name.'</a>/';
								}
							?>	
							<a href="/contact/index">Contact</a>
							
						</div>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<!--
	<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/plugins.js"></script>-->
	

	<!-- Footer Scripts
	============================================= -->
	<!--<script type="text/javascript" src="js/functions.js"></script>-->
	<?php
		if(isset($js))
		{
			foreach($js as $scr)
			{	
				echo '<script type="text/javascript" src="'.$scr.'"></script>';	
			}
		}
	?>

</body>
</html>
<section id="slider" class="slider-parallax swiper_wrapper clearfix" style="height: 550px;" data-loop="true">
	<div class="swiper-container swiper-parent">
		<div class="swiper-wrapper">
			<?php
					$db = Db::init();
					$col = $db->slider;
					$img = $col->find(array('client_id' => CLIENTID))->sort(array('time_created'=>1));
					
					foreach ($img as $val){
			?> 
			
			<div class="swiper-slide" style="background-image: url('/showfile/show?namafile=<?php echo $val['filename'].'&w=1850&h=550'; ?>'); background-position: center top;">
	    		<div class="container clearfix">
					<div class="slider-caption">
						<h2><?php echo $val['title']; ?></h2>
						<p><?php echo $val['description']?></p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
		<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
	</div>
</section>

<section id="content">
	<div class="content-wrap">
		
			<div class="fslider testimonial testimonial-full" data-arrows="false" style="z-index: 2;">
				<div class="flexslider">
					<div class="slider-wrap">
						<div class="slide">
							<div class="testi-content">
								<?php
									$db = Db::init();
									$preference = helper::getPreferences();
									$intitle = array();
									$intro = "";
									foreach ($preference['intro'] as $key) {
										$intitle = explode(" ",$key['intro_title']);
										$intro = $key['intro'];
									}
									echo '<p>'.$intro.'</p>';
								?>		
							</div>
						</div>
					</div>
				</div>
			</div>
		
	</div>
	<div class="container clearfix">
			<div class="clear-bottommargin">
				<div class="row common-height clearfix">
						<?php
							$db = Db::init();
							$pref = $db->content_image;
							$datas = $pref->find(array('client_id' => CLIENTID));
								
								foreach ($datas as $img) {
									echo '<div class="col-md-4 col-sm-6 bottommargin">
												<div class="feature-box fbox-plain">
													<div class="fbox-icon">
														<a href="#"><img src="/showfile/show?namafile='.$img['filename'].'&w=50&h=50" alt="Concrete Developments"></a>
									 				</div>
									 				<h3>'.$img['title'].'</h3>
									 				<p>'.$img['description'].'</p>
								 				</div>
										 </div>';
								}
						?>
				</div>
			</div>
	</div>
	<div class="container">
		<div class="row">
			
				<?php   
					$content = $db->content;
					$idcontent = "";
					foreach ($preference['categories'] as $keydata) {
						$idcontent = $keydata['category'];
					} 
					
					$mcon = $content->findOne(array('category_id' => $idcontent));			
					$isicontent = "";
						foreach ($mcon['content'] as $isi) {
							$isicontent = $isi['content'];
						}	
						echo '<p align="center">'.$isicontent.'</p>';
				?>
			
		</div>
	</div>
</section>
<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Postcontent
					============================================= -->
					<div class="postcontent nobottommargin">

						<h3>Send us an Email</h3>

						<div class="contact-widget">

							<div class="contact-form-result"></div>

							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="/contact/simpanData" method="post">

								<div class="form-process"></div>

								<div class="col_one_third">
									<label for="template-contactform-name">Name <small>*</small></label>
									<input type="text" id="name" name="name" value="" class="sm-form-control required" />
								</div>

								<div class="col_one_third">
									<label for="template-contactform-email">Email <small>*</small></label>
									<input type="email" id="email" name="email" value="" class="required email sm-form-control" />
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-phone">Phone</label>
									<input type="text" id="phone" name="phone" value="" class="sm-form-control" />
								</div>

								<div class="clear"></div>

								<div class="col_two_third">
									<label for="template-contactform-subject">Subject <small>*</small></label>
									<input type="text" id="subject" name="subject" value="" class="required sm-form-control" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-message">Message <small>*</small></label>
									<textarea class="required sm-form-control" id="message" name="message" rows="6" cols="30"></textarea>
								</div>

								<!--
								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
								</div>-->
								

								<div style="color:#00A550 ">
									<button class="btn btn-success" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
								</div>

							</form>
						</div>

					</div><!-- .postcontent end -->

					<!-- Sidebar
					============================================= -->
					<div class="sidebar col_last nobottommargin">

						<address>
							<strong>Headquarters:</strong><br>
							<?php 
								foreach ($data as $key) {
									echo $key['address'];
								
							?>
							<!--
							795 Folsom Ave, Suite 600<br>
														San Francisco, CA 94107<br>-->
							
						</address>
						<?php
							echo '<abbr title="Phone Number"><strong>Phone:</strong></abbr> '.$key['phone'].'<br>';
							echo '<abbr title="Email Address"><strong>Email:</strong></abbr> '.$key['email'].'';
							}
						?>
						
					</div><!-- .sidebar end -->

				</div>

			</div>
</section>